
-- SUMMARY --

This module adds a formatter for image fields, that make possible to select a hover image style.

-- REQUIREMENTS --

Field

-- INSTALLATION --

Normal Drupal module installation, see http://drupal.org/documentation/install/modules-themes/modules-7 for further information.

-- CONFIGURATION --



-- CONTACT --

Current maintainers:
* Lennard Westerveld (Cylon) - http://drupal.org/user/1828812
